package swing2;
import javax.swing.*;

public class panelHasil {
	private JFrame f = new JFrame("Second");
	
	String namaBarang, hasilTotalConvert;
	String jenisMember;
	String jumlahBarang ="0";
	int harga;
	double potongan = 0, jumHasilTotal;
	
	public void run() {
		f.setLayout(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(370, 200);
		f.setVisible(true);
		
		JLabel barangLabel = new JLabel("Nama Barang");
        barangLabel.setBounds(10,20,100,25);
        f.add(barangLabel);
        
        JLabel baranghasil = new JLabel(this.namaBarang);
        baranghasil.setBounds(130,20,210,25);
        f.add(baranghasil);
        
        JLabel jumlahLabel = new JLabel("Jumlah Barang");
        jumlahLabel.setBounds(10,50,100,25);
        f.add(jumlahLabel);
        
        JLabel jumlahHasil = new JLabel(this.jumlahBarang);
        jumlahHasil.setBounds(130,50,210,25);
        f.add(jumlahHasil);
        
        JLabel memberLabel = new JLabel("Status Member");
        memberLabel.setBounds(10,80,100,25);
        f.add(memberLabel);
        
        JLabel memberHasil = new JLabel(this.jenisMember);
        memberHasil.setBounds(130,80,210,25);
        f.add(memberHasil);
        
        JLabel totalLabel = new JLabel("Total Belanja");
        totalLabel.setBounds(10,110,100,25);
        f.add(totalLabel);
        
        //Calculate
        int total = Integer.parseInt(jumlahBarang);
        if(jenisMember == "Member") {
        	potongan = 0.1;
        }
        
        switch(namaBarang) {
        	case "PC": harga = 2000000;break;
        	case "Laptop": harga = 5000000;break;
        	default: harga = 900000;
        }
        
        jumHasilTotal = (1-potongan)*harga*total;
        String totalConvert = String.valueOf(jumHasilTotal);
        
        JLabel totalHasil = new JLabel(totalConvert);
        totalHasil.setBounds(130,110,100,25);
        f.add(totalHasil);
	}
	
}
