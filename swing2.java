package swing2;

import java.awt.event.ActionEvent;

import javax.swing.*;
import java.awt.event.*;

public class swing2 {
	
	String jenisPembelian;
	static String memberStatus;

	public static void main(String[] args) {    
        JFrame frame = new JFrame("Pilih Barang");
        frame.setSize(370, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);

        // Setting the frame visibility to true
        frame.setVisible(true);
    }
	
	private static void placeComponents(JPanel panel) {

        panel.setLayout(null);

        // Pilih barang
        JLabel barangLabel = new JLabel("Pilih Barang");
        barangLabel.setBounds(10,20,100,25);
        panel.add(barangLabel);
        
        String[] listBarang = {"PC", "Laptop", "Monitor"};
        JComboBox comboBarang = new JComboBox(listBarang);
        comboBarang.setBounds(130,20,210,25);
        panel.add(comboBarang);

    	// Jenis pembelian 
        JLabel jenisLabel = new JLabel("Jenis Pembelian");
        jenisLabel.setBounds(10,50,100,25);
        panel.add(jenisLabel);

        JRadioButton memberButton   	= new JRadioButton();
        JRadioButton nonMemberButton    = new JRadioButton();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(memberButton);
        buttonGroup.add(nonMemberButton);
        
        memberButton.setText("Member");
        nonMemberButton.setText("Non-Member");
        
        memberButton.setBounds(130,50,100,25);
        nonMemberButton.setBounds(240,50,100,25);
        
        panel.add(memberButton);
        panel.add(nonMemberButton);
        
        // Jumlah Pembelian
        JLabel jumlahLabel = new JLabel("Jumlah Pembelian");
        jumlahLabel.setBounds(10,80,120,25);
        panel.add(jumlahLabel);
        
        JTextField jumlahPembelian = new JTextField(20);
        jumlahPembelian.setBounds(130,80,210,25);
        panel.add(jumlahPembelian);
        
        // Tombol
        JButton calcButton = new JButton("Kalkulasi");
        calcButton.setBounds(10, 130, 330, 25);
        calcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(memberButton.isSelected()) {
					memberStatus = "Member";	
				}else {
					memberStatus = "Non-Member";
				}
				System.out.print(memberStatus);
				panelHasil hasil = new panelHasil();
				hasil.namaBarang = comboBarang.getSelectedItem().toString();
				hasil.jenisMember = memberStatus;
				hasil.jumlahBarang = jumlahPembelian.getText();
				hasil.run();
			}
		});
        panel.add(calcButton);
    }
	
}
